import 'reflect-metadata';
import * as bodyParser from 'body-parser';
import express from 'express';
import { getSingletonMongoInstance } from '../helpers/mongo';
import { logger } from '../helpers/logger';
import Router from '../routes';

class Application {
  private expressServer: any;


  constructor() {
    this.expressServer = express();
  }

  /**
   * Application bootstrap
   */
  public async bootstrap() {
    try {
      await this.databaseConnect();
      this.serverStart();
    } catch (error) {
      logger.error('Database connection error: ', error);
    }
  }

  /**
   * Get server
   */
  public getServer() {
    return this.expressServer;
  }

  /**
   * Start server
   */
  private serverStart() {
    // Enable CORS
    this.enableCORS('http://0.0.0.0:8080');
    this.expressServer.use(bodyParser.json());

    // register controllers and routes
    Router.register(this.expressServer);

    /**
     * Start Express server.
     */
    const PORT = process.env.PORT || 8080;

    this.expressServer.listen(PORT, () => {
      logger.info(
        '  App is running at http://0.0.0.0:%d in %s mode',
        PORT,
        this.expressServer.get('env'),
      );
      logger.info('  Press CTRL-C to stop\n');
    });
  }

  /**
   * Connect to database
   */
  private async databaseConnect() {
    return getSingletonMongoInstance();
  }

  /**
   * Enable CORS for defined url
   * @param url
   */
  private enableCORS(url: any) {
    this.expressServer.use((req: any, res: any, next: any) => {
      res.set({
        'Access-Control-Allow-Origin': url,
        'Access-Control-Allow-Credentials': true,
        'Access-Control-Allow-Methods': 'GET,HEAD,OPTIONS,POST,PUT,DELETE',
        'Access-Control-Allow-Headers':
          'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, '
          + 'Access-Control-Request-Headers, authorization, Pragma, Cache-Control, synergy-login-token, If-Modified-Since, user-id, corporate-id',
      });

      // intercept pre-flight (options method) request
      if (req.method === 'OPTIONS') {
        res.sendStatus(204);
      } else {
        next();
      }
    });
  }
}

export default new Application();
