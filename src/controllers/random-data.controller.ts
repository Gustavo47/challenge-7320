
import {
  Get, JsonController, Param, Delete, Patch, OnUndefined, Body,
} from 'routing-controllers';
import { RandomDataService } from '../services/random-data.service';
import { logger } from '../helpers/logger';

/**
 * Random Data Controller
 * Used for manipulating Random Data
 * Decorated for swagger and routing-controller
 */

@JsonController()
export class RandomDataController {
  constructor(private randomDataService: RandomDataService) {}

  // This action is not restfull, but I'm delete to prevent cleaning the just by clicking in the link
  @Delete('/api/random-data-flush')
  public async deleteAllRandomData() {
    logger.debug('Deleting all random data in the database');
    return { itemsDeleted: await this.randomDataService.deleteAllRandomData() };
  }

  @Delete('/api/random-data/:key')
  @OnUndefined(404)
  public async deleteRandomData(@Param('key') key: string) {
    logger.debug(`Deleting random data with key: ${key}`);
    const itemsDeleted = await this.randomDataService.deleteRandomData(key);
    return itemsDeleted > 0 ? { itemsDeleted } : undefined;
  }

  @Get('/api/random-data/:key')
  public getRandomData(@Param('key') key: string) {
    logger.debug(`Random Data returned with key: ${key}`);
    return this.randomDataService.getRandomData(key);
    // It could return status code 201 when create a new resouce
  }

  @Get('/api/random-data')
  public getAllRandomData() {
    logger.debug('Getting all random data in the database');
    // @TODO Pagination
    return this.randomDataService.getAllRandomData();
  }

  @Patch('/api/random-data/:key')
  @OnUndefined(404)
  public changeRandomData(@Param('key') key: string, @Body() data: {data: string}) {
    logger.debug(`Changing data of random data with key: ${key}`);
    return this.randomDataService.changeRandomData(key, data);
  }
}
