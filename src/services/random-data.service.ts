import 'reflect-metadata';
import { Inject, Service } from 'typedi';
import { logger } from '../helpers/logger';
import { createRandomData } from '../helpers/random-data.factory';
import { RandomData } from '../models/random-data';
import { RandomDataRepository } from '../repository/random-data.repository';

@Service()
export class RandomDataService {
    @Inject()
    randomDataRepository: RandomDataRepository;


    /*
     I ran out time here to implement it, but I would sort the data in the database to delete the oldest one.
    */
    // private cacheLimit = 10;

    // In Ms
    private TTL = 1000;

    // The task description is saying to just return a string, to keep a consistent api I'm returning an object
    public async getRandomData(key: string): Promise<RandomData> {
      const randomDataFromDataBase = await this.randomDataRepository.getRandomData(key);
      if (randomDataFromDataBase
        && randomDataFromDataBase.created + this.TTL >= new Date().getTime()) {
        logger.info('Cache hit');
        return randomDataFromDataBase;
      }

      logger.info('Cache miss');

      const newRandomData = createRandomData(key);
      await this.randomDataRepository.saveRandomData(newRandomData);
      return this.randomDataRepository.getRandomData(key); // To have a consistent reponse, we could just return newRandomData.
    }

    // The task description is saying to just return a string, to keep a consistent api I'm returning an object
    public async deleteRandomData(key: string): Promise<number> {
      return this.randomDataRepository.deleteRandomData(key);
    }

    public async deleteAllRandomData(): Promise<number> {
      return this.randomDataRepository.deleteAllRandomData();
    }

    public async getAllRandomData(): Promise<RandomData[]> {
      return this.randomDataRepository.getAllRandomData();
    }

    public async changeRandomData(key: string, data: {data: string}): Promise<RandomData|undefined> {
      if (data?.data?.length > 0) {
        const randomDataFromDataBase = await this.randomDataRepository.getRandomData(key);
        if (randomDataFromDataBase) {
          randomDataFromDataBase.data = data?.data;
          await this.randomDataRepository.saveRandomData(randomDataFromDataBase);
          return randomDataFromDataBase;
        }
      }
      return undefined;
    }
}
