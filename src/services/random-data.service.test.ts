import { randomDataRepositoryMock } from '../repository/random-data.repository.mock';
import { RandomDataService } from './random-data.service';

describe('Random Data Repository', () => {
  const randomDataService = new RandomDataService();
  const key = 'key';
  const data = 'data';
  let mockRepo;
  Math.random = () => 0.1;
  beforeEach(() => {
    mockRepo = randomDataRepositoryMock(key, data);
    (randomDataService as any).randomDataRepository = mockRepo;

    const DATE_TO_USE = new Date('2020');
    const _Date = Date;
    global.Date = jest.fn(() => DATE_TO_USE);
    global.Date.UTC = _Date.UTC;
    global.Date.parse = _Date.parse;
    global.Date.now = _Date.now;
  });

  describe('getRandomData', () => {
    it('should create random data and save in database if is not there', async () => {
      expect(await randomDataService.getRandomData(key)).toEqual({ key, data });

      expect(mockRepo.getRandomData).toBeCalledTimes(2);
      expect(mockRepo.getRandomData).toBeCalledWith(key);
    });

    it('should get from database', async () => {
      mockRepo.getRandomData = jest.fn(() => ({ key, data, created: new Date().getTime() }));
      expect(await randomDataService.getRandomData(key)).toEqual({ key, data, created: new Date().getTime() });

      expect(mockRepo.getRandomData).toBeCalledTimes(1);
      expect(mockRepo.getRandomData).toBeCalledWith(key);
    });

    it('should create random data and save in database if is expired', async () => {
      const ttl = 10000;
      mockRepo.getRandomData = jest.fn(() => ({ key, data, created: new Date().getTime() - ttl }));
      expect(await randomDataService.getRandomData(key)).toEqual({ key, data, created: new Date().getTime() - ttl });

      expect(mockRepo.getRandomData).toBeCalledTimes(2);
      expect(mockRepo.getRandomData).toBeCalledWith(key);
    });

    it.todo('test bad cases');
  });
  describe('deleteRandomData', () => {
    it('should pass right params to delete function', async () => {
      mockRepo.deleteRandomData = jest.fn(() => 1);
      expect(await randomDataService.deleteRandomData(key)).toEqual(1);

      expect(mockRepo.deleteRandomData).toBeCalledWith(key);
    });

    it.todo('test bad cases');
  });

  describe('deleteAllRandomData', () => {
    it('should pass right params to delete function', async () => {
      mockRepo.deleteAllRandomData = jest.fn(() => 1);
      expect(await randomDataService.deleteAllRandomData()).toEqual(1);

      expect(mockRepo.deleteAllRandomData).toBeCalledWith();
    });

    it.todo('test bad cases');
  });

  describe('getAllRandomData', () => {
    it('should pass right params to get all function', async () => {
      mockRepo.getAllRandomData = jest.fn(() => 1);
      expect(await randomDataService.getAllRandomData()).toEqual(1);

      expect(mockRepo.getAllRandomData).toBeCalledWith();
    });

    it.todo('test bad cases');
  });

  describe('changeRandomData', () => {
    it('should pass right params to save function', async () => {
      mockRepo.getAllRandomData = jest.fn(() => 1);
      expect(await randomDataService.changeRandomData(key, { data })).toEqual({ key, data });

      expect(mockRepo.getRandomData).toBeCalledWith(key);
      expect(mockRepo.saveRandomData).toBeCalledWith({ key, data });
    });
    it('should return undefined if data is not valid', async () => {
      expect(await randomDataService.changeRandomData(key, { data: '' })).toBeUndefined();
      expect(await (randomDataService as any).changeRandomData(key)).toBeUndefined();
    });
    it('should return undefined if key is not in database', async () => {
      mockRepo.getRandomData = jest.fn(() => false);
      expect(await randomDataService.changeRandomData(key, { data })).toBeUndefined();
    });

    it.todo('test bad cases');
  });
});
