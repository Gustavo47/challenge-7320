import express from 'express';
import Application from './core/application';

const app = express();
// bootstrap application
Application.bootstrap();

export default app;
