import 'reflect-metadata';
import { useExpressServer, useContainer } from 'routing-controllers';
import { Container } from 'typedi';
import { RandomDataController } from './controllers/random-data.controller';

class Router {
  private controllers: any[] = [RandomDataController];

  public register = (server: any) => {
    // route for test controller and route
    useContainer(Container);
    useExpressServer(server, {
      validation: true,
      controllers: this.controllers,
    });
  };
}

export default new Router();
