/**
 * Configuration file for database
 */
export const configDBMongo = {
  host: process.env.DB_HOST || 'localhost',
  port: 27018,
  username: process.env.DB_USER || 'root',
  password: process.env.DB_PWD || '1234',
  collection: process.env.DB_COLLECTION || 'test',
};
