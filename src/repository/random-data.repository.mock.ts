export const randomDataRepositoryMock = (key, data) => ({
  getRandomData: jest.fn(() => ({ key, data })),

  saveRandomData: jest.fn(),

  deleteRandomData: jest.fn(),

  deleteAllRandomData: jest.fn(),

  getAllRandomData: jest.fn(),
});
