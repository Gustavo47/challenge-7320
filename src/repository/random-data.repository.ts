import 'reflect-metadata';
import { Service } from 'typedi';
import { getSingletonMongoInstance } from '../helpers/mongo';
import { RandomData } from '../models/random-data';


@Service()
export class RandomDataRepository {
  public collectionName = 'random-data';

  public async getRandomData(key: string): Promise<RandomData | null> {
    const db = await getSingletonMongoInstance();

    const data = await db.collection(this.collectionName).findOne({ key });
    if (data) {
      return data;
    }
    return null;
  }

  public async saveRandomData(randomData: RandomData): Promise<boolean> {
    const db = await getSingletonMongoInstance();

    const data = await db.collection(this.collectionName).replaceOne({ key: randomData.key }, randomData, { upsert: true });
    if (data) {
      return Boolean(data.result.ok);
    }
    throw new Error('Error on saving RandomData');
  }

  public async deleteRandomData(key: string): Promise<number> {
    const db = await getSingletonMongoInstance();

    const data = await db.collection(this.collectionName).deleteMany({ key });
    if (data) {
      return data.deletedCount;
    }
    return 0;
  }

  public async deleteAllRandomData(): Promise<number> {
    const db = await getSingletonMongoInstance();

    const data = await db.collection(this.collectionName).deleteMany({}); // or .remove, but remove will not return the deleteCount
    if (data) {
      return data.deletedCount;
    }
    return 0;
  }

  public async getAllRandomData(): Promise<RandomData[]> {
    const db = await getSingletonMongoInstance();

    const data = await db.collection(this.collectionName).find({}); // or .remove, but remove will not return the deleteCount
    if (data) {
      return data.toArray();
    }
    return [];
  }
}
