import { RandomDataRepository } from './random-data.repository';
import { getSingletonMongoInstance } from '../helpers/mongo';

jest.mock('../helpers/mongo');

describe('Random Data Repository', () => {
  const randomDataRepository = new RandomDataRepository();
  const key = 'key';
  const data = 'data';

  describe('getRandomData', () => {
    it('should pass right params to mongo function', async () => {
      const findOne = jest.fn(() => ({ key, data }));
      const collection = jest.fn(() => ({ findOne }));
      (getSingletonMongoInstance as any) = jest.fn(() => ({
        collection,
      }));

      expect(await randomDataRepository.getRandomData(key)).toEqual({ key, data });

      expect(getSingletonMongoInstance).toBeCalled();
      expect(collection).toBeCalledWith(randomDataRepository.collectionName);
      expect(findOne).toBeCalledWith({ key });
    });
    it.todo('test bad cases');
  });

  describe('saveRandomData', () => {
    it('should pass right params to mongo function', async () => {
      const replaceOne = jest.fn(() => ({ result: { ok: 1 } }));
      const collection = jest.fn(() => ({ replaceOne }));
      (getSingletonMongoInstance as any) = jest.fn(() => ({
        collection,
      }));

      expect(await randomDataRepository.saveRandomData({ key, data } as any)).toEqual(true);

      expect(getSingletonMongoInstance).toBeCalled();
      expect(collection).toBeCalledWith(randomDataRepository.collectionName);
      expect(replaceOne).toBeCalledWith({ key }, { key, data }, { upsert: true });
    });
    it.todo('test bad cases');
  });

  describe('deleteAllRandomData', () => {
    it('should pass right params to mongo function', async () => {
      const deleteMany = jest.fn(() => ({ deletedCount: 1 }));
      const collection = jest.fn(() => ({ deleteMany }));
      (getSingletonMongoInstance as any) = jest.fn(() => ({
        collection,
      }));

      expect(await randomDataRepository.deleteAllRandomData()).toEqual(1);

      expect(getSingletonMongoInstance).toBeCalled();
      expect(collection).toBeCalledWith(randomDataRepository.collectionName);
      expect(deleteMany).toBeCalledWith({ });
    });
    it.todo('test bad cases');
  });
  describe('deleteRandomData', () => {
    it('should pass right params to mongo function', async () => {
      const deleteMany = jest.fn(() => ({ deletedCount: 1 }));
      const collection = jest.fn(() => ({ deleteMany }));
      (getSingletonMongoInstance as any) = jest.fn(() => ({
        collection,
      }));

      expect(await randomDataRepository.deleteRandomData(key)).toEqual(1);

      expect(getSingletonMongoInstance).toBeCalled();
      expect(collection).toBeCalledWith(randomDataRepository.collectionName);
      expect(deleteMany).toBeCalledWith({ key });
    });
    it.todo('test bad cases');
  });
  describe('getAllRandomData', () => {
    it('should pass right params to mongo function', async () => {
      const find = jest.fn(() => ({ toArray: () => [1] }));
      const collection = jest.fn(() => ({ find }));
      (getSingletonMongoInstance as any) = jest.fn(() => ({
        collection,
      }));

      expect(await randomDataRepository.getAllRandomData()).toEqual([1]);

      expect(getSingletonMongoInstance).toBeCalled();
      expect(collection).toBeCalledWith(randomDataRepository.collectionName);
      expect(find).toBeCalledWith({ });
    });
    it.todo('test bad cases');
  });
});
