import { createRandomData } from './random-data.factory';

describe('RandomData Factory', () => {
  const DATE_TO_USE = new Date('2020');
  const _Date = Date;
  global.Date = jest.fn(() => DATE_TO_USE);
  global.Date.UTC = _Date.UTC;
  global.Date.parse = _Date.parse;
  global.Date.now = _Date.now;

  Math.random = () => 0.1;

  it('should create a random data object', () => {
    const key = 'key';
    const expected = { key, created: 1577836800000, data: 'lllllm' };

    expect(createRandomData(key)).toEqual(expected);
  });
});
