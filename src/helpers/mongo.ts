import { MongoClient, Db } from 'mongodb';
import { configDBMongo } from '../config/db.config';

const db: Db | null = null;
export const getSingletonMongoInstance = async (): Promise<Db> => {
  if (db) {
    return db;
  }
  // @TODO Implement auth
  const url = `mongodb://${configDBMongo.host}:${configDBMongo.port}`;
  const client = await MongoClient.connect(url);

  return client.db(configDBMongo.collection);
};
