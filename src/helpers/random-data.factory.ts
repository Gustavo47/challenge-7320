import { RandomData } from '../models/random-data';

export const createRandomData = (key: string): RandomData => {
  const data = Math.random().toString(36).substring(7);
  const created = new Date().getTime();
  return ({ key, data, created });
};
