import Pino, { Level, PrettyOptions } from 'pino';

const prettyPrintOptions: PrettyOptions = { ignore: 'pid,hostname,time,v' };

const enabled = true;
const level: Level = (process.env.LOG_LEVEL as Level) || 'info';
const prettyPrint = process.env.ENV !== 'production' ? prettyPrintOptions : false;

export const log = Pino({
  name: 'fashion-cloud-server',
  level,
  prettyPrint,
  enabled,
});

// Alias of 'log' to make importing easier
export const logger = log;
