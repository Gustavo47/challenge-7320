/**
 * RamdomData model
 * Describes ramdom data type
 */
export interface RandomData {
  key: string;
  data: string;
  created: number;
}
