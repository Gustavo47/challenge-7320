module.exports = {
  clearMocks: true,
  transform: { '^.+\\.ts?$': 'ts-jest' },
  globals: { 'ts-jest': { isolatedModules: true } },
  collectCoverage: true,
  collectCoverageFrom: [
    './src/**/*.ts',
    '!**/node_modules/**',
  ],
  coverageDirectory: './coverage',
  coverageReporters: ['json', 'lcov', 'text'],
  testEnvironment: 'node',
  preset: 'ts-jest',
  testMatch: ['<rootDir>/**/*.(spec|test).ts'],
  moduleFileExtensions: ['ts', 'js'],
  testPathIgnorePatterns: ['node_modules/', 'dist/'],
};
